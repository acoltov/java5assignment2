//package com.andriancoltov.data;
//
//import com.andriancoltov.beans.Player;
//import com.andriancoltov.web.PlayerContainer;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcOperations;
//import org.springframework.stereotype.Repository;
//
//import javax.swing.tree.RowMapper;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
///**
// * Created by Alienware on 2015-08-12.
// */
//
//@Repository
//public class JdbcPlayerRepository implements PlayerRepository {
//
//    private JdbcOperations jdbc;
//
//    @Autowired
//    public JdbcPlayerRepository(JdbcOperations jdbc) {
//        this.jdbc = jdbc;
//    }
//
//    public PlayerContainer save(PlayerContainer playerContainer) {
//        jdbc.update(
//          "INSERT INTO Player (first_name, last_name, age, country, position, salary, goals, bookings)" +
//                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
//                playerContainer.getFirstName(),
//                playerContainer.getLastName(),
//                playerContainer.getAge(),
//                playerContainer.getCountryOfBirth(),
//                playerContainer.getPosition(),
//                playerContainer.getAnnualSalary(),
//                playerContainer.getNumberOfGoals(),
//                playerContainer.getNumberOfBookings());
//        return playerContainer;
//    }
//
//    public PlayerContainer findPlayerByFirstAndSecondName(String firstName, String lastName) {
//        return jdbc.queryForObject(
//                "select first_name, last_name, age, country, position, salary, goals, bookings from Player where first_name=? and last_name=?",
//                new PlayerContainerRowMapper(),
//                firstName, lastName);
//    }
//
//    private static class PlayerContainerRowMapper implements RowMapper<PlayerContainer> {
//        public PlayerContainer mapRow(ResultSet rs, int rowNum) throws SQLException {
//            return new PlayerContainer(
//                    rs.getString("first_name"),
//                    rs.getString("last_name"),
//                    rs.getInt("age"),
//                    rs.getString("country"),
//                    rs.("position"),
//                    rs.getBigDecimal("salary"),
//                    rs.getInt("goals"),
//                    rs.getInt("bookings"));
//        }
//    }
//}
