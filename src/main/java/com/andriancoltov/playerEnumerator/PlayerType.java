package com.andriancoltov.playerEnumerator;

/**
 * Created by Alienware on 2015-07-26.
 */
public enum PlayerType {
    goalkeeper, defender, midfielder, forward
}
