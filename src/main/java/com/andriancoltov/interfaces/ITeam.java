package com.andriancoltov.interfaces;

import com.andriancoltov.beans.Player;
import com.andriancoltov.beans.Trainer;

import java.util.List;

/**
 * Created by Alienware on 2015-07-27.
 */
public interface ITeam {

    List<Player> playersList = null;
    Trainer trainer = null;
    String teamName = null;
    int foundationYear = 0;
}
