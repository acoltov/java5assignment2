package com.andriancoltov.web;

import com.andriancoltov.beans.Player;
import com.andriancoltov.beans.Statistics;
import com.andriancoltov.factory.FactoryPlayer;
import com.andriancoltov.playerEnumerator.PlayerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.validation.Valid;
import java.util.Currency;
import java.util.Locale;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/Player")
public class PlayerController {

//  private SpitterRepository spitterRepository;
//
//  @Autowired
//  public PlayerController(SpitterRepository spitterRepository) {
//    this.spitterRepository = spitterRepository;
//  }


  private PlayerContainer playerContainer;
  private Player player;
  private Currency currency = Currency.getInstance(Locale.US);

  @Autowired
  private FactoryPlayer factoryPlayer;

  @Autowired
  public PlayerController(PlayerContainer playerContainer) {
    this.playerContainer = playerContainer;
  }
  
  @RequestMapping(value="/create", method=GET)
  public String showCreatePlayerForm(Model model) {
    model.addAttribute(new PlayerContainer());
    return "createPlayerForm";
  }

  @RequestMapping(value="/create", method=POST)
  public String processCreatePlayer(
      @Valid PlayerContainer playerContainer,
      Errors errors) {
    if (errors.hasErrors()) {
      return "createPlayerForm";
    }

//    spitterRepository.save(spitter);

    player = factoryPlayer.createPlayer(playerContainer.getFirstName(), playerContainer.getLastName(),
            playerContainer.getAge(), playerContainer.getCountryOfBirth(), playerContainer.getPosition(),
            playerContainer.getAnnualSalary(), currency,
            new Statistics(playerContainer.getNumberOfGoals(), playerContainer.getNumberOfBookings()));

//    System.out.println(player.toString());

    if (player == null) {
      return "createPlayerForm";
    }
    else {
      return "redirect:/Player/" + player.getPosition();
    }
  }

  @RequestMapping(value="/{position}", method=GET)
  public String showCreatedPlayer(@PathVariable PlayerType position, Model model) {
    model.addAttribute(player);

    System.out.println(model.toString());

    return "showPlayer";
  }
  
}
