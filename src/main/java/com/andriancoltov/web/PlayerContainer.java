package com.andriancoltov.web;

import com.andriancoltov.playerEnumerator.PlayerType;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;
import java.math.BigDecimal;

/**
 * Created by Alienware on 2015-08-09.
 */

@Component
public class PlayerContainer {

    @NotNull
    @Size(min=3, max=16, message = "{firstName.size}")
    @Pattern(regexp = "^[A-Za-z ]++$", message = "{firstName.regexp}")
    private String firstName;

    @NotNull
    @Size(min=3, max=16, message = "{lastName.size}")
    @Pattern(regexp = "^[A-Za-z ]++$", message = "{lastName.regexp}")
    private String lastName;

    @NotNull(message = "{age.player}")
    @Min(21)
    @Max(23)
    private int age;

    @NotNull
    @Size(min=5, max=16, message = "{countryOfBirth.size}")
    private String countryOfBirth;

    @NotNull
    private PlayerType position;

    @NotNull
    @Min(value = 30000,message = "{salary.size}")
    private BigDecimal annualSalary;

    @NotNull
    @Min(value = 0,message = "{numberOfGoals.size}")
    private int numberOfGoals;

    @NotNull
    @Min(value = 0,message = "{numberOfBookings.size}")
    private int numberOfBookings;

    public PlayerContainer() {
    }

    public PlayerContainer(String firstName, String lastName, int age, String countryOfBirth, PlayerType position, BigDecimal annualSalary, int numberOfGoals, int numberOfBookings) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.countryOfBirth = countryOfBirth;
        this.position = position;
        this.annualSalary = annualSalary;
        this.numberOfGoals = numberOfGoals;
        this.numberOfBookings = numberOfBookings;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public PlayerType getPosition() {
        return position;
    }

    public void setPosition(PlayerType position) {
        this.position = position;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(BigDecimal annualSalary) {
        this.annualSalary = annualSalary;
    }

    public int getNumberOfGoals() {
        return numberOfGoals;
    }

    public void setNumberOfGoals(int numberOfGoals) {
        this.numberOfGoals = numberOfGoals;
    }

    public int getNumberOfBookings() {
        return numberOfBookings;
    }

    public void setNumberOfBookings(int numberOfBookings) {
        this.numberOfBookings = numberOfBookings;
    }

    @Override
    public String toString() {
        return "PlayerContainer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", countryOfBirth='" + countryOfBirth + '\'' +
                ", position=" + position +
                ", annualSalary=" + annualSalary +
                ", numberOfGoals=" + numberOfGoals +
                ", numberOfBookings=" + numberOfBookings +
                '}';
    }
}
