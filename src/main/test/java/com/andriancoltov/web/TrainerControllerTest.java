package com.andriancoltov.web;

import com.andriancoltov.config.RootOrganisationConfig;
import com.andriancoltov.factory.FactoryTrainer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Alienware on 2015-08-10.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {WebConfig.class,RootOrganisationConfig.class})
public class TrainerControllerTest {

    @Autowired
    FactoryTrainer factoryTrainer;

    @Test
    public void testTrainerCreatePage() throws Exception {
        TrainerContainer trainerContainer= new TrainerContainer();
        TrainerController controller = new TrainerController(trainerContainer);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/Trainer/create"))
                .andExpect(view().name("createTrainerForm"));
    }

    @Test
    public void testTrainerShowPage() throws Exception {
//      TrainerContainer trainerContainer= new TrainerContainer("Super", "Maaaaan", 41, new BigDecimal("2500000"));
        TrainerContainer mockContainer = mock(TrainerContainer.class);
        TrainerController controller = new TrainerController(mockContainer);
        MockMvc mockMvc = standaloneSetup(controller).build();


        mockMvc.perform(post("/Trainer/create")
                .param("firstName", "Super")
                .param("lastName", "Maaaaan")
                .param("age", "41")
                .param("annualSalary", "250000"))
                .andExpect(redirectedUrl("/Trainer/Super"));
//            .andExpect(view().name("showTrainer"));
//            .andExpect(view().name("createTrainerForm"));
    }

}
