package com.andriancoltov.web;

import com.andriancoltov.beans.Player;
import com.andriancoltov.beans.Statistics;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Alienware on 2015-08-10.
 */

public class PlayerControllerTest {

  @Test
  public void testPlayerCreatePage() throws Exception {
    PlayerContainer playerContainer= new PlayerContainer();
    PlayerController controller = new PlayerController(playerContainer);
    MockMvc mockMvc = standaloneSetup(controller).build();
    mockMvc.perform(get("/Player/create"))
           .andExpect(view().name("createPlayerForm"));
  }

  @Test
  public void testPlayerShowPage() throws Exception {
    PlayerContainer playerContainer= new PlayerContainer();
    PlayerController controller = new PlayerController(playerContainer);
    MockMvc mockMvc = standaloneSetup(controller).build();


    mockMvc.perform(post("/Player/create")
           .param("firstName", "Super")
           .param("lastName", "Maaaan")
           .param("age", "22")
           .param("countryOfBirth", "Canada")
           .param("position", "goalkeeper")
           .param("annualSalary", "250000")
           .param("numberOfGoals", "3")
           .param("numberOfBookings", "2")
            )
            .andExpect(view().name("/Player/goalkeeper"));
  }

}
