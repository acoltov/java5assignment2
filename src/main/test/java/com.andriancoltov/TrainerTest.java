package com.andriancoltov;

import com.andriancoltov.beans.*;
import com.andriancoltov.config.RootOrganisationConfig;
import com.andriancoltov.factory.FactoryTrainer;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Alienware on 2015-07-26.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RootOrganisationConfig.class})
public class TrainerTest {

    private static String validFirstName;
    private static String validLastName;
    private static int validAge;
    private static BigDecimal validAnnualSalary;
    private static Currency validCurrency;

    @BeforeClass
    public static void prepData() {
        validFirstName = "John";
        validLastName = "Smith";
        validAge = 40;
        validAnnualSalary = new BigDecimal("3000000");
        validCurrency = Currency.getInstance(Locale.US);
    }

    @Autowired
    private FactoryTrainer factoryTrainer;

    @Test
    public void FactoryTrainerNotNull() {
        assertNotNull(factoryTrainer);
    }

    @Test
    public void createValidTrainer() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, validLastName, validAge, validAnnualSalary, validCurrency);
        assertNotNull(trainer);
    }

    @Test
    public void createInvalidTrainerWrongFirstName() {
        Trainer trainer = factoryTrainer.createTrainer("test 5", validLastName, validAge, validAnnualSalary, validCurrency);
        assertNull(trainer);
    }

    @Test
    public void createInvalidTrainerWrongLastName() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, "", validAge, validAnnualSalary, validCurrency);
        assertNull(trainer);
    }

    @Test
    public void createInvalidTrainerWrongAge() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, validLastName, 39, validAnnualSalary, validCurrency);
        assertNull(trainer);
    }

    @Test
    public void createInvalidTrainerWrongAnnualSalary() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, validLastName, validAge, new BigDecimal("0"), validCurrency);
        assertNull(trainer);
    }
}
