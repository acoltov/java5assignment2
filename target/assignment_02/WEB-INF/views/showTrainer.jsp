<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
  <title>Trainer Details</title>
  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />" >
</head>
<body>
<h1>Trainer Details</h1>
<table border=0 cellpadding=0>
  <tr>
    <td>First Name (*):</td>
    <td><c:out value="${trainer.firstName}" /></td>
  </tr>
  <tr>
    <td>Last Name (*):</td>
    <td><c:out value="${trainer.lastName}" /></td>
  </tr>
  <tr>
    <td>Age (*):</td>
    <td><c:out value="${trainer.age}" /></td>
  </tr>
  <tr>
    <td>Annual salary (*):</td>
    <td><c:out value="${trainer.annualSalary} ${trainer.currency}" /></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><button type="button" onclick="document.location='/assignment_02/'">Return</button></td>
  </tr>
</table>
</body>
</html>
