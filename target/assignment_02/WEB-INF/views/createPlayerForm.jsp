<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page session="false" %>
<html>
<head>
    <title>CreatePlayer</title>
    <link rel="stylesheet" type="text/css"
          href="<c:url value="/resources/style.css" />" >
</head>
<body>
<h1>Create Player</h1>

<sf:form method="POST" commandName="playerContainer">
    <sf:errors path="*" element="div" cssClass="error"/>
    <table border=0 cellpadding=0>
        <tr>
            <td>First Name (*):</td>
            <td><sf:input path="firstName" cssErrorClass="error"/></td>
        </tr>
        <tr>
            <td>Last Name (*):</td>
            <td><sf:input path="lastName"  cssErrorClass="error"/></td>
        </tr>
        <tr>
            <td>Age (*):</td>
            <td><sf:input path="age"  cssErrorClass="error"/></td>
        </tr>
        <tr>
            <td>Country of birth (*):</td>
            <td>
                <sf:select path="countryOfBirth"  cssErrorClass="error">
                    <sf:option value="">none</sf:option>
                    <sf:option value="Canada">Canada</sf:option>
                    <sf:option value="USA">U.S.A.</sf:option>
                    <sf:option value="Russia">Russia</sf:option>
                    <sf:option value="Spain">Spain</sf:option>
                </sf:select>
            </td>
        </tr>
        <tr>
            <td>Position (*):</td>
            <td>
                <select name="position">
                    <option value="">none</option>
                    <option value="goalkeeper">goalkeeper</option>
                    <option value="defender">defender</option>
                    <option value="midfielder">midfielder</option>
                    <option value="forward">forward</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Annual salary (*):</td>
            <td><sf:input path="annualSalary"  cssErrorClass="error"/></td>
        </tr>
        <tr>
            <td colspan="2">Stats (*):</td>
        </tr>
        <tr>
            <td>Number of goals (*)</td>
            <td><sf:input path="numberOfGoals"  cssErrorClass="error"/></td>
        </tr>
        <tr>
            <td>Number of bookings (*):</td>
            <td><sf:input path="numberOfBookings"  cssErrorClass="error"/></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="reset" value="Reset" /> |
                <button type="button" onclick="document.location='/assignment_02/'">Cancel</button> |
                <input type="submit" value="Create" />
            </td>
        </tr>
    </table>

</sf:form>
</body>
</html>
