<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
  <head>
    <title>TeamManagement</title>
    <link rel="stylesheet" 
          type="text/css" 
          href="<c:url value="/resources/style.css" />" >
  </head>
  <body>
    <h1>Welcome to Team Management</h1>

    <a href="<c:url value="/Player/create" />">Create Player</a> |
    <a href="<c:url value="/Trainer/create" />">Create Trainer</a> |
    <%--<a href="<c:url value="/createTeam" />">Create Team</a>--%>
  </body>
</html>
